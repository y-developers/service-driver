[![Release](https://jitpack.io/v/com.gitlab.y-developers/service-driver.svg)]
(https://jitpack.io/#com.gitlab.y-developers/service-driver)
# Java "Service Driver"

## Using this project

You can use this project via gradle, maven, sbt and leiningen, by requiring it as dependency from jitpack.
[More info](https://jitpack.io/#com.gitlab.y-developers/service-driver)

## About this Project
This project is made, to provide you a way to manage your services and its dependencies in Java with the help of a single 
(or multiple) xml file(s). *You can find a simple example in the "example" folder*.

Besides this small example, you also have some more advanced options. So you can not only inject services directly, but 
also build factories, so that you can control the creation of your service. You can also define variables inside the scope
of your XML file or directly pass a String as an argument. 

If you want to use a [Factory](https://en.wikipedia.org/wiki/Factory_method_pattern), 
the factory class has to implement the `de.yannik_sc.java.y_services.lib.IFactory` interface and define by that, which
service the factory will build.

A very basic Factory could that look like that:

```
package me.test;

import de.yannik_sc.java.y_services.lib.IFactory;

class TestFactory implements IFactory<TestService> {
    public TestService getService() {
        return new TestService();
    }
}
```

### Arguments
There are four types of arguments:
- [service](#service)
- [factory](#factory)
- [variable](#variable)
- [value](#value)

#### service
The service argument receives the id of a defined service, and a service is defined by using the `service` xml tag.
The service id has to be passed into the `service` attribute of the argument

#### factory
A factory, gets defined by using the `factory` tag, like the service, this tag requires the `class` and the `id` attribute.
The factory id has to be passed into the `factory` attribute of the argument

#### variable
The variable argument, accepts the id of a defined variable, by using the `variable` tag, which then contains a string 
in its XML body.
The variable id has to be passed into the `variable` attribute of the argument

#### value
The value, gets passed to the argument, by putting it into the tag body. It will be passed as a string to the service

package de.yannik_sc.java.y_services.impl;

import de.yannik_sc.java.y_services.impl.structs.*;
import de.yannik_sc.java.y_services.lib.IServiceXmlParser;
import de.yannik_sc.java.y_services.lib.exceptions.InvalidXmlException;
import de.yannik_sc.java.y_services.lib.structs.*;
import de.yannik_sc.java.y_services.lib.types.ArgumentTypeEnum;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * This class parses given xml files which are containing a valid format
 *
 * @author Yannik_Sc.
 */
public class ServiceXmlParserImpl implements IServiceXmlParser
{
    DocumentBuilder builder;
    ClassLoader classLoader;

    public ServiceXmlParserImpl() throws ParserConfigurationException
    {
        this(ClassLoader.getSystemClassLoader());
    }

    public ServiceXmlParserImpl(ClassLoader classLoader) throws ParserConfigurationException
    {
        this.classLoader = classLoader;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        this.builder = factory.newDocumentBuilder();
    }

    @Override
    public IServicesStruct parseXml(String xml) throws IOException, SAXException, InvalidXmlException, ClassNotFoundException
    {
        return this.parse(this.builder.parse(xml));
    }

    @Override
    public IServicesStruct parseXml(File xml) throws IOException, SAXException, InvalidXmlException, ClassNotFoundException
    {
        return this.parse(this.builder.parse(xml));
    }

    @Override
    public IServicesStruct parseXml(InputStream xml) throws IOException, SAXException, InvalidXmlException, ClassNotFoundException
    {
        return this.parse(this.builder.parse(xml));
    }

    protected IServicesStruct parse(Document document) throws InvalidXmlException, ClassNotFoundException
    {
        if (!"services".equals(document.getDocumentElement().getTagName())) {
            throw new InvalidXmlException(document.getDocumentURI());
        }

        IServicesStruct services = new ServicesStructImpl();
        Node root = document.getChildNodes().item(0);

        for (int i = 0; i < root.getChildNodes().getLength(); i++) {
            Node child = root.getChildNodes().item(i);

            if ("#text".equals(child.getNodeName())) {
                continue;
            }

            if ("service".equals(child.getNodeName())) {
                services.addService(this.buildService(child));

                continue;
            }

            if ("factory".equals(child.getNodeName())) {
                services.addFactory(this.buildFactory(child));

                continue;
            }

            if ("variable".equals(child.getNodeName())) {
                services.addVariable(new VariableStructImpl(
                        this.getAttributeOrDefault("id", "", child),
                        child.getTextContent()
                ));

                continue;
            }

            throw new InvalidXmlException(document.getDocumentURI());
        }

        return services;
    }

    protected IFactoryStruct buildFactory(Node node) throws ClassNotFoundException, InvalidXmlException
    {
        Class clazz;
        String id;
        boolean autobuild;

        try {
            clazz = Class.forName(node.getAttributes().getNamedItem("class").getNodeValue(), false, this.classLoader);
            id = node.getAttributes().getNamedItem("id").getNodeValue();
        } catch (NullPointerException e) {
            throw new InvalidXmlException(node.getOwnerDocument().getDocumentURI());
        }


        autobuild = Objects.equals(this.getAttributeOrDefault("autobuild", "false", node), "true");

        FactoryStructImpl struct = new FactoryStructImpl(clazz, id);
        struct.setAutobuild(autobuild);
        this.insertArguments(node, struct);

        return struct;
    }

    protected IServiceStruct buildService(Node node) throws ClassNotFoundException, InvalidXmlException
    {
        Class clazz;
        String id;
        boolean autobuild;

        try {
            clazz = Class.forName(node.getAttributes().getNamedItem("class").getNodeValue(), false, this.classLoader);
            id = node.getAttributes().getNamedItem("id").getNodeValue();
        } catch (NullPointerException e) {
            throw new InvalidXmlException(node.getOwnerDocument().getDocumentURI());
        }

        autobuild = Objects.equals(this.getAttributeOrDefault("autobuild", "false", node), "true");

        ServiceStructImpl struct = new ServiceStructImpl(clazz, id);
        struct.setAutobuild(autobuild);
        this.insertArguments(node, struct);

        return struct;
    }

    protected void insertArguments(Node node, IStructContainsArguments struct) throws InvalidXmlException
    {
        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
            Node argument = node.getChildNodes().item(i);

            if ("#text".equals(argument.getNodeName())) {
                continue;
            }

            if (!"argument".equals(argument.getNodeName())) {
                throw new InvalidXmlException(node.getOwnerDocument().getDocumentURI());
            }

            struct.addArgument(this.getArgument(argument));
        }
    }

    protected IArgumentStruct getArgument(Node node) throws InvalidXmlException
    {
        String type;

        try {
            type = node.getAttributes().getNamedItem("type").getNodeValue();

        } catch (NullPointerException e) {
            throw new InvalidXmlException(node.getOwnerDocument().getDocumentURI());
        }

        ArgumentStructImpl argument = null;

        if (type.equals("service")) {
            argument = new ArgumentStructImpl(ArgumentTypeEnum.SERVICE);
            argument.setBuildNew(this.getAttributeOrDefault("buildNew", "false", node).equals("true"));
            argument.setService(this.getAttributeOrDefault("service", "", node));
        }

        if (type.equals("variable")) {
            argument = new ArgumentStructImpl(ArgumentTypeEnum.VARIABLE);
            argument.setVariable(this.getAttributeOrDefault("variable", "", node));
        }

        if (type.equals("value")) {
            argument = new ArgumentStructImpl(ArgumentTypeEnum.VALUE);
            argument.setContent(node.getTextContent());
        }

        if (type.equals("factory")) {
            argument = new ArgumentStructImpl(ArgumentTypeEnum.FACTORY);
            argument.setFactory(this.getAttributeOrDefault("factory", "", node));
        }

        if (argument == null) {
            throw new InvalidXmlException(node.getOwnerDocument().getDocumentURI());
        }

        return argument;
    }

    protected String getAttributeOrDefault(String attribute, String def, Node node)
    {
        Node attributeNode;

        if ((attributeNode = node.getAttributes().getNamedItem(attribute)) != null) {
            return attributeNode.getNodeValue();
        }

        return def;
    }
}

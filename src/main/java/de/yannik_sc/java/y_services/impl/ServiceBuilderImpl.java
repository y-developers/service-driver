package de.yannik_sc.java.y_services.impl;

import de.yannik_sc.java.y_services.lib.IFactory;
import de.yannik_sc.java.y_services.lib.IServiceBuilder;
import de.yannik_sc.java.y_services.lib.IServiceProvider;
import de.yannik_sc.java.y_services.lib.exceptions.*;
import de.yannik_sc.java.y_services.lib.structs.*;
import de.yannik_sc.java.y_services.lib.types.ArgumentTypeEnum;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This file is responsible for building services, defined in a
 *
 * @author Yannik_Sc.
 */
public class ServiceBuilderImpl implements IServiceBuilder
{
    protected IServicesStruct struct;
    protected Map<String, Object> builtServices = new HashMap<>();
    protected Map<String, IFactory> builtFactories = new HashMap<>();
    protected IServiceProvider provider = null;

    public ServiceBuilderImpl(IServicesStruct struct)
    {
        this.struct = struct;
    }

    @Override
    public void autoBuild() throws ServicesException
    {
        this.autoBuildServices();
        this.autoBuildFactories();
    }

    @Override
    public void setServiceProvider(IServiceProvider provider)
    {
        this.provider = provider;
    }

    @Override
    public void addService(String id, Object instance)
    {
        this.builtServices.put(id, instance);
    }

    @Override
    public Object buildService(String id) throws ServicesException
    {
        return this.buildService(id, false);
    }

    @Override
    public Object buildService(String id, boolean skipProvider) throws ServicesException
    {
        Object service = null;

        if (this.builtServices.containsKey(id)) {
            return this.builtServices.get(id);
        }

        try {
            service = this.buildFromStruct(this.getServiceStruct(id));
        } catch (ServiceNotFoundException ignored) {
        }

        if (service == null) {
            try {
                service = this.buildFromFactory(this.struct.getFactory(id));
            } catch (FactoryNotFoundException ignored) {
            }
        }

        if (service == null && !skipProvider) {
            service = this.getServiceFromProvider(id, this);
        }

        if (service == null) {
            throw new ServiceNotFoundException(id);
        }

        return service;
    }

    @Override
    public IServiceStruct getServiceStruct(String id) throws ServicesException
    {
        return this.struct.getService(id);
    }

    @Override
    public Object buildFromStruct(IServiceStruct struct) throws ServicesException
    {
        Class clazz = struct.getServiceClass();

        if (this.builtServices.containsKey(struct.getId())) {
            return this.builtServices.get(struct.getId());
        }

        List<Object> arguments;

        arguments = this.getArgumentValues(struct);

        Object service = this.createInstance(clazz, arguments);

        this.builtServices.put(struct.getId(), service);

        return service;
    }

    @Override
    public IFactory buildFactory(String id) throws ServicesException
    {
        return this.buildFactory(
            this.struct.getFactory(id)
        );
    }

    /**
     * @param factory
     * @return
     * @throws UnableToConstructObjectException
     * @throws UnableToResolveArgumentException
     */
    public IFactory buildFactory(IFactoryStruct factory) throws ServicesException
    {
        List<Object> arguments;

        if (this.builtFactories.containsKey(factory.getId())) {
            return this.builtFactories.get(factory.getId());
        }

        arguments = this.getArgumentValues(factory);

        IFactory factoryInstance = (IFactory) this.createInstance(factory.getFactoryClass(), arguments);

        this.builtFactories.put(factory.getId(), factoryInstance);

        return factoryInstance;
    }

    /**
     * @param struct
     * @return
     * @throws UnableToResolveArgumentException
     * @throws UnableToConstructObjectException
     */
    protected Object buildFromFactory(IFactoryStruct struct) throws ServicesException
    {
        return this.buildFactory(struct).getService();
    }

    /**
     * @param clazz
     * @param arguments
     * @return
     * @throws UnableToConstructObjectException
     */
    protected Object createInstance(Class clazz, List<Object> arguments) throws UnableToConstructObjectException
    {
        Object instance;

        for (Constructor constructor : clazz.getConstructors()) {
            try {
                instance = constructor.newInstance(arguments.toArray());

                return instance;
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                System.err.println(e.getMessage());
            }
        }

        for (Object argument : arguments) {
            System.err.println(argument.getClass().getName());
        }

        throw new UnableToConstructObjectException(clazz.getName());
    }

    /**
     * @param id
     * @return
     * @throws VariableNotFoundException
     */
    protected String getVariable(String id) throws VariableNotFoundException
    {
        return this.struct.getVariable(id).getContent();
    }

    /**
     * @param struct
     * @return
     * @throws UnableToResolveArgumentException
     * @throws UnableToConstructObjectException
     */
    protected List<Object> getArgumentValues(IStructContainsArguments struct) throws ServicesException
    {
        List<Object> arguments = new ArrayList<>();

        for (IArgumentStruct argument : struct.getArguments()) {
            arguments.add(this.getArgumentValue(argument));
        }

        return arguments;
    }

    /**
     * @param argument
     * @return
     * @throws VariableNotFoundException
     * @throws FactoryNotFoundException
     * @throws ServiceNotFoundException
     * @throws NoServiceConstructorFound
     * @throws UnableToResolveArgumentException
     * @throws UnableToConstructObjectException
     */
    protected Object getArgumentValue(IArgumentStruct argument) throws ServicesException
    {
        if (argument.getType() == ArgumentTypeEnum.VALUE) {
            return argument.getContent();
        }

        if (argument.getType() == ArgumentTypeEnum.VARIABLE) {
            return this.getVariable(argument.getVariable());
        }

        if (argument.getType() == ArgumentTypeEnum.FACTORY) {
            return this.buildFactory(this.struct.getFactory(argument.getFactory())).getService();
        }

        if (argument.getType() == ArgumentTypeEnum.SERVICE) {
            return this.buildService(argument.getService());
        }

        return null;
    }

    /**
     * @throws NoServiceConstructorFound
     * @throws UnableToResolveArgumentException
     * @throws UnableToConstructObjectException
     */
    protected void autoBuildServices() throws ServicesException
    {
        List<IServiceStruct> services = this.getAutoBuildServices();

        for (IServiceStruct service : services) {
            this.builtServices.put(service.getId(), this.buildFromStruct(service));
        }
    }

    /**
     * @throws NoServiceConstructorFound
     * @throws UnableToResolveArgumentException
     * @throws UnableToConstructObjectException
     */
    protected void autoBuildFactories() throws ServicesException
    {
        List<IFactoryStruct> factories = this.getAutoBuildFactories();

        for (IFactoryStruct factory : factories) {
            this.builtServices.put(factory.getId(), this.buildFactory(factory).getService());
        }
    }

    /**
     * @return
     */
    protected List<IServiceStruct> getAutoBuildServices()
    {
        List<IServiceStruct> services = new ArrayList<>();

        for (IServiceStruct service : this.struct.getServices()) {
            if (service.isAutobuild()) {
                services.add(service);
            }
        }

        return services;
    }

    /**
     * @return
     */
    protected List<IFactoryStruct> getAutoBuildFactories()
    {
        List<IFactoryStruct> services = new ArrayList<>();

        for (IFactoryStruct factory : this.struct.getFactories()) {
            if (factory.isAutobuild()) {
                services.add(factory);
            }
        }

        return services;
    }

    /**
     * @param id
     * @param serviceBuilder
     * @return
     * @throws ServiceNotFoundException
     */
    protected Object getServiceFromProvider(
        String id,
        ServiceBuilderImpl serviceBuilder
    ) throws ServiceNotFoundException
    {
        if (this.provider == null) {
            return null;
        }

        return this.provider.getService(id, serviceBuilder);
    }
}

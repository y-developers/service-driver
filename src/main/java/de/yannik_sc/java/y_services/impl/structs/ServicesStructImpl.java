package de.yannik_sc.java.y_services.impl.structs;

import de.yannik_sc.java.y_services.lib.exceptions.FactoryNotFoundException;
import de.yannik_sc.java.y_services.lib.exceptions.ServiceNotFoundException;
import de.yannik_sc.java.y_services.lib.exceptions.VariableNotFoundException;
import de.yannik_sc.java.y_services.lib.structs.IFactoryStruct;
import de.yannik_sc.java.y_services.lib.structs.IServiceStruct;
import de.yannik_sc.java.y_services.lib.structs.IServicesStruct;
import de.yannik_sc.java.y_services.lib.structs.IVariableStruct;

import java.util.ArrayList;
import java.util.List;

/**
 * This file represents the "services" xml tag
 *
 * @author Yannik_Sc.
 */
public class ServicesStructImpl implements IServicesStruct
{
    List<IServiceStruct> services = new ArrayList<>();
    List<IFactoryStruct> factories = new ArrayList<>();
    List<IVariableStruct> variables = new ArrayList<>();

    @Override
    public void addService(IServiceStruct service)
    {
        this.services.add(service);
    }

    @Override
    public void addFactory(IFactoryStruct factory)
    {
        this.factories.add(factory);
    }

    @Override
    public void addVariable(IVariableStruct variable)
    {
        this.variables.add(variable);
    }

    @Override
    public List<IServiceStruct> getServices()
    {
        return this.services;
    }

    @Override
    public List<IFactoryStruct> getFactories()
    {
        return this.factories;
    }

    @Override
    public List<IVariableStruct> getVariables()
    {
        return this.variables;
    }

    @Override
    public IServiceStruct getService(String id) throws ServiceNotFoundException
    {
        for (IServiceStruct service : this.services) {
            if(id.equals(service.getId())) {
                return service;
            }
        }

        throw new ServiceNotFoundException(id);
    }

    @Override
    public IFactoryStruct getFactory(String id) throws FactoryNotFoundException
    {
        for (IFactoryStruct factory : this.factories) {
            if(id.equals(factory.getId())) {
                return factory;
            }
        }

        throw new FactoryNotFoundException(id);
    }

    @Override
    public IVariableStruct getVariable(String id) throws VariableNotFoundException
    {
        for (IVariableStruct variable : this.variables) {
            if(id.equals(variable.getId())) {
                return variable;
            }
        }

        throw new VariableNotFoundException(id);
    }
}

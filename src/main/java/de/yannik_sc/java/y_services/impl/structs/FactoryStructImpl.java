package de.yannik_sc.java.y_services.impl.structs;

import de.yannik_sc.java.y_services.lib.structs.IArgumentStruct;
import de.yannik_sc.java.y_services.lib.structs.IFactoryStruct;

import java.util.ArrayList;
import java.util.List;

/**
 * This file represents the "factory" xml tag
 *
 * @author Yannik_Sc.
 */
public class FactoryStructImpl implements IFactoryStruct
{
    protected List<IArgumentStruct> arguments = new ArrayList<>();
    protected Class factoryClass;
    protected String id;
    protected boolean autobuild = false;

    public FactoryStructImpl(Class factoryClass, String id)
    {
        this.factoryClass = factoryClass;
        this.id = id;
    }

    public void setAutobuild(boolean autobuild)
    {
        this.autobuild = autobuild;
    }

    @Override
    public void addArgument(IArgumentStruct argument)
    {
        this.arguments.add(argument);
    }

    @Override
    public List<IArgumentStruct> getArguments()
    {
        return this.arguments;
    }

    @Override
    public Class getFactoryClass()
    {
        return this.factoryClass;
    }

    @Override
    public String getId()
    {
        return this.id;
    }

    @Override
    public boolean isAutobuild()
    {
        return this.autobuild;
    }
}

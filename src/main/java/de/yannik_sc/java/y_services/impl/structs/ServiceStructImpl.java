package de.yannik_sc.java.y_services.impl.structs;

import de.yannik_sc.java.y_services.lib.structs.IArgumentStruct;
import de.yannik_sc.java.y_services.lib.structs.IServiceStruct;

import java.util.ArrayList;
import java.util.List;

/**
 * This file represents the "service" xml tag
 *
 * @author Yannik_Sc.
 */
public class ServiceStructImpl implements IServiceStruct
{
    protected List<IArgumentStruct> arguments = new ArrayList<>();
    protected boolean autobuild = false;
    protected Class serviceClass;
    protected String id;

    public ServiceStructImpl(Class serviceClass, String id)
    {
        this.serviceClass = serviceClass;
        this.id = id;
    }

    public void setAutobuild(boolean autobuild)
    {
        this.autobuild = autobuild;
    }

    @Override
    public void addArgument(IArgumentStruct argument)
    {
        this.arguments.add(argument);
    }

    @Override
    public List<IArgumentStruct> getArguments()
    {
        return this.arguments;
    }

    @Override
    public boolean isAutobuild()
    {
        return this.autobuild;
    }

    @Override
    public Class getServiceClass()
    {
        return this.serviceClass;
    }

    @Override
    public String getId()
    {
        return this.id;
    }
}

package de.yannik_sc.java.y_services.impl.structs;

import de.yannik_sc.java.y_services.lib.structs.IVariableStruct;

/**
 * This file represents the "variable" xml tag
 *
 * @author Yannik_Sc.
 */
public class VariableStructImpl implements IVariableStruct
{
    protected String id;
    protected String content;

    public VariableStructImpl(String id, String content)
    {
        this.id = id;
        this.content = content;
    }

    @Override
    public String getId()
    {
        return this.id;
    }

    @Override
    public String getContent()
    {
        return this.content;
    }
}

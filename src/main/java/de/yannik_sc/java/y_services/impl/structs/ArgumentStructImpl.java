package de.yannik_sc.java.y_services.impl.structs;

import de.yannik_sc.java.y_services.lib.structs.IArgumentStruct;
import de.yannik_sc.java.y_services.lib.types.ArgumentTypeEnum;

/**
 * This file represents the "argument" xml tag
 *
 * @author Yannik_Sc.
 */
public class ArgumentStructImpl implements IArgumentStruct
{
    protected String content = "";
    protected String variable = "";
    protected String service = "";
    protected String factory = "";
    protected boolean buildNew = false;
    protected ArgumentTypeEnum type;

    public ArgumentStructImpl(ArgumentTypeEnum type)
    {
        this.type = type;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public void setService(String service)
    {
        this.service = service;
    }

    public void setVariable(String variable)
    {
        this.variable = variable;
    }

    public void setFactory(String factory)
    {
        this.factory = factory;
    }

    public void setBuildNew(boolean buildNew)
    {
        this.buildNew = buildNew;
    }

    @Override
    public String getContent()
    {
        return this.content;
    }

    @Override
    public ArgumentTypeEnum getType()
    {
        return this.type;
    }

    @Override
    public String getVariable()
    {
        return this.variable;
    }

    @Override
    public String getService()
    {
        return this.service;
    }

    @Override
    public String getFactory()
    {
        return this.factory;
    }

    @Override
    public boolean isBuildNew()
    {
        return this.buildNew;
    }
}

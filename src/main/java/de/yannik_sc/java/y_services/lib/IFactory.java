package de.yannik_sc.java.y_services.lib;

/**
 * This interface defines a factory
 *
 * @author Yannik_Sc.
 */
public interface IFactory<T>
{
    /**
     * Returns the service, which it should build
     *
     * @return
     */
    T getService();
}

package de.yannik_sc.java.y_services.lib;

import de.yannik_sc.java.y_services.lib.exceptions.*;
import de.yannik_sc.java.y_services.lib.structs.IServiceStruct;

/**
 * This file describes the service builder
 *
 * @author Yannik_Sc.
 */
public interface IServiceBuilder
{
    /**
     * Builds the service behind the given id, uses factories, variables, values and other services as parameters
     *
     * @param id the identifier of the service
     * @return the new service instance
     * @throws ServiceNotFoundException in case no service was found for the given identifier
     */
    Object buildService(String id) throws ServicesException;
    /**
     * Builds the service behind the given id, uses factories, variables, values and other services as parameters
     *
     * @param id the identifier of the service
     * @param skipProvider
     * @return the new service instance
     * @throws ServiceNotFoundException in case no service was found for the given identifier
     */
    Object buildService(String id, boolean skipProvider) throws ServicesException;

    /**
     * Builds a factory
     *
     * @param id the identifier of the factory
     * @return the built factory
     * @throws ServicesException
     */
    IFactory buildFactory(String id) throws ServicesException;

    /**
     * Returns the service struct of the given identifier
     *
     * @param id the identifier of the service
     * @return the service struct that was found
     * @throws ServiceNotFoundException in case no service was found for the given identifier
     */
    IServiceStruct getServiceStruct(String id) throws ServicesException;

    /**
     * @return the service built from the given struct
     * @throws UnableToResolveArgumentException
     * @throws NoServiceConstructorFound
     * @throws UnableToConstructObjectException
     */
    Object buildFromStruct(IServiceStruct struct) throws ServicesException;

    /**
     * Manually injects service as loaded service
     *
     * @param id
     * @param instance
     */
    void addService(String id, Object instance);

    /**
     * Creates services and factories marked with the "autobuild=true" argument
     *
     * @throws UnableToResolveArgumentException
     * @throws UnableToConstructObjectException
     * @throws NoServiceConstructorFound
     */
    void autoBuild() throws ServicesException;

    /**
     * Sets a service provider
     *
     * @param provider the provider which has to provide services as a second try
     */
    void setServiceProvider(IServiceProvider provider);
}

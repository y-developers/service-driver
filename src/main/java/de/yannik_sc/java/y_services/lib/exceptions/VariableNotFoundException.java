package de.yannik_sc.java.y_services.lib.exceptions;

/**
 * This exception gets thrown, when an expected variable can't be found
 *
 * @author Yannik_Sc.
 */
public class VariableNotFoundException extends ServicesException
{
    public VariableNotFoundException(String variableId)
    {
        super(String.format("Could not find variable %s", variableId));
    }
}

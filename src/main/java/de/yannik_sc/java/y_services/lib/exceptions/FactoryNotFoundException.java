package de.yannik_sc.java.y_services.lib.exceptions;

/**
 * This exception gets thrown, when the service builder can't find an expected factory
 *
 * @author Yannik_Sc.
 */
public class FactoryNotFoundException extends ServicesException
{
    public FactoryNotFoundException(String factoryId)
    {
        super(String.format("Could not find factory %s", factoryId));
    }
}

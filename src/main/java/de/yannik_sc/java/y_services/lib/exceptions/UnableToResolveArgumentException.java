package de.yannik_sc.java.y_services.lib.exceptions;

/**
 * This exceptions gets thrown, if a given argument can't be resolved
 *
 * @author Yannik_Sc.
 */
public class UnableToResolveArgumentException extends ServicesException
{
    public UnableToResolveArgumentException(String problem, String cause)
    {
        super(String.format("Could not resolve argument %s by %s", problem, cause));
    }

    public UnableToResolveArgumentException(String problem)
    {
        super(String.format("Could not resolve argument %s", problem));
    }

    public UnableToResolveArgumentException()
    {
        super("Could not resolve arguments");
    }
}

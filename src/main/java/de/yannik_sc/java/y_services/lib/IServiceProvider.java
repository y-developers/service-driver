package de.yannik_sc.java.y_services.lib;

import de.yannik_sc.java.y_services.lib.exceptions.ServiceNotFoundException;

/**
 * This file is part of the Y_Frame
 *
 * @author Yannik_Sc.
 */
public interface IServiceProvider
{
    /**
     * @param id
     * @param serviceBuilder
     * @return
     * @throws ServiceNotFoundException
     */
    Object getService(String id, IServiceBuilder serviceBuilder) throws ServiceNotFoundException;
}

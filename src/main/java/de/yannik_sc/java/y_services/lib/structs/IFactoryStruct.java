package de.yannik_sc.java.y_services.lib.structs;

import de.yannik_sc.java.y_services.lib.IFactory;

/**
 * This file describes the "factory" xml tag
 *
 * @author Yannik_Sc.
 */
public interface IFactoryStruct<T extends IFactory> extends IStructContainsArguments
{
    /**
     * @return the factory class
     */
    Class<T> getFactoryClass();

    /**
     * @return the identifier of this factory
     */
    String getId();

    /**
     * @return weather the factory should be built by initialization or when its needed
     */
    boolean isAutobuild();
}

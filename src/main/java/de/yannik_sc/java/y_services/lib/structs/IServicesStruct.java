package de.yannik_sc.java.y_services.lib.structs;

import de.yannik_sc.java.y_services.lib.exceptions.FactoryNotFoundException;
import de.yannik_sc.java.y_services.lib.exceptions.ServiceNotFoundException;
import de.yannik_sc.java.y_services.lib.exceptions.VariableNotFoundException;

import java.util.List;

/**
 * This file describes the "services" xml tag
 *
 * @author Yannik_Sc.
 */
public interface IServicesStruct
{
    /**
     * Adds a service to this struct
     *
     * @param service
     */
    void addService(IServiceStruct service);

    /**
     * Adds a service factory to this struct
     *
     * @param factory
     */
    void addFactory(IFactoryStruct factory);

    /**
     * Adds a variable to this struct
     *
     * @param variable
     */
    void addVariable(IVariableStruct variable);

    /**
     * Returns all registered services
     *
     * @return
     */
    List<IServiceStruct> getServices();

    /**
     * Returns all registered service factories
     *
     * @return
     */
    List<IFactoryStruct> getFactories();

    /**
     * Returns all registered Variables
     *
     * @return
     */
    List<IVariableStruct> getVariables();

    /**
     * Returns the service which has the given id
     *
     * @param id
     * @return
     */
    IServiceStruct getService(String id) throws ServiceNotFoundException;

    /**
     * Returns the service factory which has the given id
     *
     * @param id
     * @return
     */
    IFactoryStruct getFactory(String id) throws FactoryNotFoundException;

    /**
     * Returns the variable which has the given id
     *
     * @param id
     * @return
     */
    IVariableStruct getVariable(String id) throws VariableNotFoundException;
}

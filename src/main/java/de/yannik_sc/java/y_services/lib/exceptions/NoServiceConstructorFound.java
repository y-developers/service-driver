package de.yannik_sc.java.y_services.lib.exceptions;

/**
 * This exception gets thrown if no working service constructor was found
 *
 * @author Yannik_Sc.
 */
public class NoServiceConstructorFound extends ServicesException
{
    public NoServiceConstructorFound(String serviceId)
    {
        super(String.format("Could not build service %s", serviceId));
    }
}

package de.yannik_sc.java.y_services.lib.exceptions;

/**
 * This exception gets thrown, if the service parser detects an invalid format
 *
 * @author Yannik_Sc.
 */
public class InvalidXmlException extends ServicesException
{
    public InvalidXmlException(String filename)
    {
        super(String.format("Invalid xml structure in file %s", filename));
    }
}

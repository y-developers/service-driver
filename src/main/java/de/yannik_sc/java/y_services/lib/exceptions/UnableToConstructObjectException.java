package de.yannik_sc.java.y_services.lib.exceptions;

/**
 * This exception gets thrown, when an argument cannot be constructed
 *
 * @author Yannik_Sc.
 */
public class UnableToConstructObjectException extends ServicesException
{
    public UnableToConstructObjectException(String objectName)
    {
        super(String.format("Unable to construct object %s", objectName));
    }
}

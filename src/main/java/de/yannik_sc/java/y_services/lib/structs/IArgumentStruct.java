package de.yannik_sc.java.y_services.lib.structs;

import de.yannik_sc.java.y_services.lib.types.ArgumentTypeEnum;

/**
 * This file describes the "argument" xml tag
 *
 * @author Yannik_Sc.
 */
public interface IArgumentStruct
{
    /**
     * @return the node content
     */
    String getContent();

    /**
     * @return the node type
     */
    ArgumentTypeEnum getType();

    /**
     * @return the node variable attribute
     */
    String getVariable();

    /**
     * @return the node service attribute
     */
    String getService();

    /**
     * @return the node factory attribute
     */
    String getFactory();

    /**
     * @return weather to use a new instance or use an already existing instance
     */
    boolean isBuildNew();
}

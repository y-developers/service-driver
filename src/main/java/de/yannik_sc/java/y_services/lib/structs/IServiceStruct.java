package de.yannik_sc.java.y_services.lib.structs;

/**
 * This file describes the "service" xml tag
 *
 * @author Yannik_Sc.
 */
public interface IServiceStruct<T> extends IStructContainsArguments
{
    /**
     * @return weather the service should be build on load or not
     */
    boolean isAutobuild();

    /**
     * @return the class of the service
     */
    Class<T> getServiceClass();

    /**
     * @return the id of the service
     */
    String getId();
}

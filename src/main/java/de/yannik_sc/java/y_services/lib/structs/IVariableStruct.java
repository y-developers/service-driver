package de.yannik_sc.java.y_services.lib.structs;

/**
 * This file describes the "service" xml tag
 *
 * @author Yannik_Sc.
 */
public interface IVariableStruct
{
    /**
     * @return the identifier of the variable
     */
    String getId();

    /**
     * @return the node content
     */
    String getContent();
}

package de.yannik_sc.java.y_services.lib.exceptions;

/**
 * This exception bundles all service related exceptions
 *
 * @author Yannik_Sc.
 */
public class ServicesException extends Exception
{
    public ServicesException(String message)
    {
        super(message);
    }
}

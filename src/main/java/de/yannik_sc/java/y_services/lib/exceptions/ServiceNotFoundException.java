package de.yannik_sc.java.y_services.lib.exceptions;

/**
 * This exception gets thrown, when no service with an expected id was found
 *
 * @author Yannik_Sc.
 */
public class ServiceNotFoundException extends ServicesException
{
    public ServiceNotFoundException(String serviceId)
    {
        super(String.format("Could not find service with id %s", serviceId));
    }
}

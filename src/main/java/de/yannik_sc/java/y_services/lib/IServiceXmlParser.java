package de.yannik_sc.java.y_services.lib;

import de.yannik_sc.java.y_services.lib.exceptions.InvalidXmlException;
import de.yannik_sc.java.y_services.lib.structs.IServicesStruct;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class describes the xml parser
 *
 * @author Yannik_Sc.
 */
public interface IServiceXmlParser
{
    /**
     * Parses the xml file from the given path
     *
     * @param xml
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws InvalidXmlException
     * @throws ClassNotFoundException
     */
    IServicesStruct parseXml(String xml) throws IOException, SAXException, InvalidXmlException, ClassNotFoundException;

    /**
     * Parses the given xml file
     *
     * @param xml
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws InvalidXmlException
     * @throws ClassNotFoundException
     */
    IServicesStruct parseXml(File xml) throws IOException, SAXException, InvalidXmlException, ClassNotFoundException;

    /**
     * Parses xml from a given input stream
     *
     * @param xml
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws InvalidXmlException
     * @throws ClassNotFoundException
     */
    IServicesStruct parseXml(InputStream xml) throws IOException, SAXException, InvalidXmlException, ClassNotFoundException;
}

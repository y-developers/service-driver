package de.yannik_sc.java.y_services.lib.types;

/**
 * This enum contains valid argument types
 *
 * @author Yannik_Sc.
 */
public enum ArgumentTypeEnum
{
    SERVICE("service"),
    VALUE("value"),
    VARIABLE("variable"),
    FACTORY("factory");

    private final String type;

    ArgumentTypeEnum(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return this.type;
    }
}

package de.yannik_sc.java.y_services.lib.structs;

import java.util.List;

/**
 * This interface defines if a tag accepts arguments
 *
 * @author Yannik_Sc.
 */
public interface IStructContainsArguments
{
    /**
     * @param argument the argument node that should be added
     */
    void addArgument(IArgumentStruct argument);

    /**
     * @return the argument nodes
     */
    List<IArgumentStruct> getArguments();
}

package test;

import de.yannik_sc.java.y_services.impl.ServiceBuilderImpl;
import de.yannik_sc.java.y_services.impl.ServiceXmlParserImpl;
import de.yannik_sc.java.y_services.lib.IServiceBuilder;
import de.yannik_sc.java.y_services.lib.IServiceXmlParser;
import de.yannik_sc.java.y_services.lib.exceptions.ServicesException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * @author Yannik_Sc
 */
public class Main
{
    public static void main(String[] args) throws ClassNotFoundException, SAXException, ServicesException, IOException, ParserConfigurationException
    {
        IServiceXmlParser parser = new ServiceXmlParserImpl();
        IServiceBuilder builder = new ServiceBuilderImpl(parser.parseXml(String.valueOf(ClassLoader.getSystemResource(
            "services.xml"))));
        builder.autoBuild();
        TestService service = (TestService) builder.buildService("test.TestService");
        service.test();
    }
}

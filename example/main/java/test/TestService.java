package test;

/**
 * @author Yannik_Sc
 */
public class TestService
{
    protected TestDependency dependency;

    public TestService(TestDependency dependency)
    {
        this.dependency = dependency;
    }

    public void test()
    {
        this.dependency.test();
    }
}
